INTRODUCTION
------------

The Optasy Many Select module provides a better widget for  Drupal 8 Core
multi select fields.

REQUIREMENTS
------------

This module requires the following modules:

 * Field UI (Drupal Core)
 * Options (Drupal Core)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

HOW TO USE
------------

 * Go to Entity Form Display and select "Many select list(s)", configure it.


CONFIGURATION
-------------
No configs required.


TROUBLESHOOTING
---------------

MAINTAINERS
-----------
  * Lilian Catanoi (liliancatanoi90) - https://www.drupal.org/u/liliancatanoi90

This project is sponsored by:
 * [OPTASY](https://www.optasy.com) is a Canadian Drupal development & web
  development company based in Toronto. In the past we provided Drupal
  development solutions for a variety of Canadian and foregin companies with
  outstanding results.
